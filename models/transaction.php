<?php
  use \Curl\Curl;

  class Transaction {

    // we define the attributes
    // they are public so that we can access them using $post->author directly
    public $id;
    public $product_id;
    public $user_id;
    public $transaction_code;
    public $status;
    public $amount;
    public $source;
    public $medium;
    public $campaign;
    public $database;

    public function __construct($transaction) {
      $this->id = $transaction["id"];
      $this->product_id = $transaction["product_id"];
      $this->user_id = $transaction["user_id"];
      $this->transaction_code = $transaction["transaction_code"];
      $this->status = $transaction["status"];
      $this->amount = $transaction["amount"];
      $this->source = $transaction["source"];
      $this->medium = $transaction["medium"];
      $this->campaign = $transaction["campaign"];
    }

    public static function find_by_code($transaction_code) {
      $database = Db::getInstance();
      $transaction = $database->get("transactions", [
        "id",
      ], [
        "transaction_code" => $transaction_code
      ]);
      return $transaction['id'];
    }

    // returns transaction code from pagseguro
    public static function start($product_id) {
      // get product
      $product = Product::find($product_id);

      // send data to pagseguro
      $data = array(
        'token' => '1FAF18908CAA468CA2389EBE7F604E0B',
        'email' => 'financeiro@pipedigital.com',
        'currency' => 'BRL',
        'itemId1' => $product->sku,
        'itemQuantity1' => '1',
        'itemDescription1' => $product->name,
        'itemAmount1' => $product->price,
      );
      $url = "https://ws.sandbox.pagseguro.uol.com.br/v2/checkout/";

      $response = new Curl();
      $response->post($url, $data);

      if ($response->error) {
          echo 'Error: '.$response->errorCode;
      } else {
        $transaction_code = json_decode(json_encode((array)$response->response->code), TRUE);

        // return transaction code and selected product
        return $array = array(
          "transaction_code" => $transaction_code,
          "product" => $product
        );
      }

    }

    // creates record in database when transaction is finished
    public static function create($product_id, $transaction_code, $source, $medium, $campaign) {
      $transaction = array(
        "transaction_code" => (string)$transaction_code,
        "product_id" => $product_id,
        "user_id" => null,
        "status" => 'opened',
        "amount" => '1',
        "source" => $source,
        "medium" => $medium,
        "campaign" => $campaign
      );

      $database = Db::getInstance();
      $database->insert("transactions", $transaction);
      $transaction_id = $database->id();

      $id = array("id" => $transaction_id);
      $transaction = array_merge($transaction, $id);

      // return created transaction using model
      return new Transaction($transaction);
    }

    // update transaction when pagseguro notify
    public static function update($notification_type, $notification_code) {
      // send notification code to pagseguro
      $email = 'financeiro@pipedigital.com';
      $token = '1FAF18908CAA468CA2389EBE7F604E0B';

      $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/notifications/' . $_POST['notificationCode'] . '?email=' . $email . '&token=' . $token;
      $response = new Curl();
      $response->get($url);

      if ($response->error) {
          echo 'Error: '.$response->errorCode;
      } else {
        $response = json_decode(json_encode((array)$response->response), TRUE);

        // if the transaction is payed
        if ($response['status'] == 3) {
          // select transaction to update and verify if user exists
          $transaction_id = self::find_by_code($response['code']);
          $user_id = User::find_by_email($response['sender']['email']);

          // if user doesn't exist create one
          if ($user_id == null) {
            $user = array(
              "name" => $response['sender']['name'],
              "email" => $response['sender']['email'],
              "phone" => $response['sender']['phone']['areaCode'].$response['sender']['phone']['number'],
              "city" => $response['shipping']['address']['city'],
              "state" => $response['shipping']['address']['state'],
              "country" => $response['shipping']['address']['country']
            );
            $user = User::create($user);
            $user_id = $user->id;
          }

          // update transaction with user id and status
          $database = Db::getInstance();
          $transaction = $database->update("transactions", [
            "user_id" => $user_id,
            "status" => "payed"
          ], [
            "id" => $transaction_id
          ]);

          // send fiscal document to bling
          $url = 'https://bling.com.br/Api/v2/notafiscal/json/';
          $postal_code = $response['shipping']['address']['postalCode'];
          $formated_cep = substr($postal_code , 0 , 2). ".".substr($postal_code , 2 , 3)."-" .substr($postal_code , 5 , 9);

          $xml = '<?xml version="1.0" encoding="UTF-8"?><pedido>
            <cliente>
              <nome>'.$response['sender']['name'].'</nome>
              <tipo>F</tipo>
              <endereco>'.$response['shipping']['address']['street'].'</endereco>
              <numero>'.$response['shipping']['address']['number'].'</numero>
              <bairro>'.$response['shipping']['address']['district'].'</bairro>
              <cep>'.$formated_cep.'</cep>
              <cidade>'.$response['shipping']['address']['city'].'</cidade>
              <uf>'.$response['shipping']['address']['state'].'</uf>
              <email>'.$response['sender']['email'].'</email>
            </cliente>
            <itens>
              <item>
                <descricao>'.$response['items']['item']['description'].'</descricao>
                <codigo>'.$response['items']['item']['id'].'</codigo>
                <un>cx</un>
                <qtde>'.$response['items']['item']['quantity'].'</qtde>
                <vlr_unit>'.$response['items']['item']['amount'].'</vlr_unit>
                <tipo>P</tipo>
                <origem>0</origem>
              </item>
            </itens>
            </pedido>';

          $response = new Curl();
          $response->post($url, array(
            'apikey' => '61cd4edb8edbc4c2e179f0a64edfa78fbf87b729',
            'xml' => rawurlencode($xml),
          ));
        }
      }
    }
  }
?>
