<?php
  class Product {
    // we define the attributes
    // they are public so that we can access them using $post->author directly
    public $id;
    public $sku;
    public $name;
    public $category;
    public $price;
    public $database;

    public function __construct($product) {
      $this->id = $product['id'];
      $this->sku = $product['sku'];
      $this->name = $product['name'];
      $this->category = $product['category'];
      $this->price = $product['price'];
    }

    public static function find($id) {
      // we make sure $id is an integer
      $id = intval($id);

      $database = Db::getInstance();
      $product = $database->get("products", [
        "id",
      	"sku",
      	"name",
        "category",
        "price"
      ], [
      	"id" => $id
      ]);
      $product_id = $database->id();
      $id = array("id" => $product_id);
      $product = array_merge($product, $id);

      return new Product($product);
    }
  }
?>
