<?php
  class User {
    // we define the attributes
    // they are public so that we can access them using $post->author directly
    public $id;
    public $name;
    public $email;
    public $phone;
    public $city;
    public $state;
    public $country;
    public $database;

    public function __construct($user) {
      $this->id = $user['id'];
      $this->name = $user['name'];
      $this->email = $user['email'];
      $this->phone = $user['phone'];
      $this->city = $user['city'];
      $this->state = $user['state'];
      $this->country = $user['country'];
    }

    public static function find_by_email($email) {
      $database = Db::getInstance();
      if ($database->has("users", ["AND" => ["email" => $email] ])) {
        $user_id = $database->get("users", "id", [
          "email" => $email
        ]);
        return $user_id;
      } else {
        return null;
      }
    }

    public static function create($user) {
      $user = array(
        "name" => $user['name'],
        "email" => $user['email'],
        "phone" => $user['phone'],
        "city" => $user['city'],
        "state" => $user['state'],
        "country" => $user['country']
      );

      $database = Db::getInstance();
      $database->insert("users", $user);
      $user_id = $database->id();

      $id = array("id" => $user_id);
      $user = array_merge($user, $id);

      // return created user using model
      return new User($user);
    }
  }
?>
