<?php
  require_once 'vendor/autoload.php';
  require_once 'connection.php';

  if (isset($_GET['controller']) && isset($_GET['action'])) {
    $controller = $_GET['controller'];
    $action     = $_GET['action'];
  } else {
    $controller = 'pages';
    $action     = 'home';
  }

  $production = false;
  $project_slug = 'template';
  $ga = 'UA-102556460-1';
  $gtm = 'GTM-W6WHD2Z';

  if ($production == true) {
      define('URL','' );
  } else {
    define('URL','http://'.$_SERVER['SERVER_NAME'].'/'.$project_slug.'/' );
  }
  define('SITE', URL);
  define('CSS', URL.'assets/css/');
  define('JS', URL.'assets/js/');
  define('IMG', URL.'assets/img/');

  if ($controller == 'transactions') {
    require_once 'routes.php';
  } else {
    require_once 'views/layout.php';
  };
?>
