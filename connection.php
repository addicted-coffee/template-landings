<?php
  use Medoo\Medoo;
  error_reporting(E_ALL);

  class Db {
    private static $database = NULL;
    private function __construct() {}
    private function __clone() {}
    public static function getInstance() {
      // Initialize
      $database = new Medoo([
        'database_type' => 'mysql',
        'database_name' => 'bodyrip',
        'server' => 'localhost',
        'username' => 'root',
        'password' => ''
      ]);

      return $database;
    }
  }
?>
