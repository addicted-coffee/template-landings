<?php
  function call($controller, $action) {
    require_once('controllers/' . $controller . '_controller.php');

    switch($controller) {
      case 'pages':
        $controller = new PagesController();
      break;
      case 'transactions':
        // we need the model to query the database later in the controller
        require_once('models/transaction.php');
        require_once('models/product.php');
        require_once('models/user.php');
        $controller = new TransactionsController();
      break;
      case 'products':
        // we need the model to query the database later in the controller
        require_once('models/product.php');
        $controller = new ProductsController();
      break;
    }

    $controller->{ $action }();
  }

  // we're adding an entry for the new controller and its actions
  $controllers = array( 'pages' => ['home', 'thank_you', 'error'],
                        'transactions' => ['start', 'create', 'update'],
                        'products' => ['show']);

  if (array_key_exists($controller, $controllers)) {
    if (in_array($action, $controllers[$controller])) {
      call($controller, $action);
    } else {
      call('pages', 'error');
    }
  } else {
    call('pages', 'error');
  }
?>
