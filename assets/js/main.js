var site_url = '/template',
		trackerName;

$(document).ready(function(){
	$('.buy-button').click(function(e) {
		trackPage('carrinho');

		var url = site_url + '?controller=transactions&action=start&product_id=' + $(this).data('id');
		$.get(url, function(data) {
			var data = JSON.parse(data),
					product = data.product;
			PagSeguroLightbox({
				code: data.transaction_code[0]
			}, {
				success: function(transactionCode) {
					trackPage('checkout');
					createTransaction(product, transactionCode);
				}
			});
    });
	})

});

function createTransaction(product, transactionCode) {
	var url = site_url + '?controller=transactions&action=create&product_id=' + product.id + '&transaction_code=' + transactionCode + '&source=' + getParam('utm_source') + '&medium=' + getParam('utm_medium') + '&campaign=' + getParam('utm_campaign');
	$.get(url, function(data) {
		trackPage('obrigado');

		var transaction = JSON.parse(data);

		trackerName = ga.getAll()[0].get('name');
		ga(trackerName + '.require', 'ecommerce');
		ga(trackerName + '.ecommerce:clear');

		ga(trackerName + '.ecommerce:addTransaction', {
		  'id': transaction.id,
		  'revenue': product.price,
		});

		ga(trackerName + '.ecommerce:addItem', {
		  'id': product.id,
		  'name': product.name,
		  'sku': product.sku,
		  'category': product.category,
		  'price': product.price,
		  'quantity': 1
		});

		ga(trackerName + '.ecommerce:send');
	});
}

function trackPage(url) {
	trackerName = ga.getAll()[0].get('name');
	ga(trackerName + '.set', 'page', site_url + '/' + url);
	ga(trackerName + '.send', 'pageview');
}

function pushEvent(category, label, location) {
	trackerName = ga.getAll()[0].get('name');
	ga(trackerName + '.send', {
		hitType: 'event',
		eventCategory: category,
		eventAction: 'click',
		eventLabel: label,
    'hitCallback': function() {
      if (location != undefined) {
        window.location = location;
      }
    }
	});
}

function getParam(paramenter) {
	var results = new RegExp('[\?&]' + paramenter + '=([^&#]*)').exec(window.location.href);
	if (results == null){
		return null;
	}
	else{
		return decodeURI(results[1]) || 0;
	}
}
