<?php
  class ProductsController {
    public function show() {
      // we expect a url of form ?controller=products&action=show&id=x
      $product = Product::find($_GET['id']);
      require_once('views/products/show.php');
    }
  }
?>
