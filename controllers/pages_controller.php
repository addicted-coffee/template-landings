<?php
  class PagesController {
    public function home() {
      require_once('views/pages/home.php');
    }

    public function thank_you() {
      require_once('views/pages/thank_you.php');
    }

    public function error() {
      require_once('views/pages/error.php');
    }
  }
?>
