<?php
  class TransactionsController {
    public function start() {
      // we expect a url of form ?controller=transactions&action=start&product_id=x&
      $transaction = Transaction::start($_GET['product_id']);
      echo json_encode($transaction, JSON_PRETTY_PRINT);
    }

    public function create() {
      // we expect a url of form ?controller=transactions&action=create&transaction_id=x&transaction_code=x
      $transaction = Transaction::create($_GET['product_id'], $_GET['transaction_code'], $_GET['source'], $_GET['medium'], $_GET['campaign']);
      echo json_encode($transaction, JSON_PRETTY_PRINT);
    }

    public function update() {
      // we expect a url of form ?controller=transactions&action=update
      if (isset($_POST['notificationType']) && isset($_POST['notificationCode']) && $_POST['notificationType'] == 'transaction') {
        $transaction = Transaction::update($_POST['notificationType'], $_POST['notificationCode']);

        print_r($transaction);
      }
    }
  }
?>
